﻿using UnityEngine;
using System.Collections;

public class test_StateChanger : MonoBehaviour 
{
    vp_FPCamera myTest;
    void Awake()
    {
        myTest = transform.GetComponent<vp_FPCamera>();
    }
	
	void Start () 
	{
	
	}
	
	
	void Update () 
	{

        if (Input.GetKeyDown(KeyCode.H))
            myTest.SetState("TestState", true);
        if (Input.GetKeyUp(KeyCode.H))
            myTest.SetState("TestState", false);
	}
}
