﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;

namespace DunGen
{
	public static class UnityUtil
	{
		public static string GetUniqueName(string name, IEnumerable<string> usedNames)
		{
			if(string.IsNullOrEmpty(name))
				return GetUniqueName("New", usedNames);
			
			string baseName = name;
			int number = 0;
			bool hasNumber = false;

			int indexOfLastSeperator = name.LastIndexOf(' ');

			if(indexOfLastSeperator > -1)
			{
				baseName = name.Substring(0, indexOfLastSeperator);
				hasNumber = int.TryParse(name.Substring(indexOfLastSeperator + 1), out number);
				number++;
			}

			foreach(var n in usedNames)
			{
				if(n == name)
				{
					if(hasNumber)
						return GetUniqueName(baseName + " " + number.ToString(), usedNames);
					else
						return GetUniqueName(name + " 2", usedNames);
				}
			}
			
			return name;
		}

        public static Bounds CalculateObjectBounds(GameObject obj, bool includeInactive, bool ignoreSpriteRenderers)
        {
            Bounds bounds = new Bounds();
            bool hasBounds = false;

            foreach (var renderer in obj.GetComponentsInChildren<Renderer>(includeInactive))
            {
                bool considerRenderer = (renderer is MeshRenderer) || ((renderer is SpriteRenderer) && !ignoreSpriteRenderers);

                if (!considerRenderer)
                    continue;

                if (hasBounds)
                    bounds.Encapsulate(renderer.bounds);
                else
                    bounds = renderer.bounds;

                hasBounds = true;
            }

            foreach (var collider in obj.GetComponentsInChildren<Collider>(includeInactive))
            {
                if (hasBounds)
                    bounds.Encapsulate(collider.bounds);
                else
                    bounds = collider.bounds;

                hasBounds = true;
            }

            return bounds;
        }

        public static IEnumerable<T> GetComponentsInParents<T>(GameObject obj, bool includeInactive = false) where T : Component
        {
            if (obj.activeSelf || includeInactive)
            {
                foreach (var comp in obj.GetComponents<T>())
                    yield return comp;
            }

            if (obj.transform.parent != null)
                foreach (var comp in GetComponentsInParents<T>(obj.transform.parent.gameObject, includeInactive))
                    yield return comp;
        }

        public static T GetComponentInParents<T>(GameObject obj, bool includeInactive = false) where T : Component
        {
            if (obj.activeSelf || includeInactive)
            {
                foreach (var comp in obj.GetComponents<T>())
                    return comp;
            }

            if (obj.transform.parent != null)
                return GetComponentInParents<T>(obj.transform.parent.gameObject, includeInactive);
            else
                return null;
        }
	}
}
