﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace DunGen.Editor
{
    [CustomEditor(typeof(DungeonArchetype))]
	public sealed class DungeonArchetypeInspector : UnityEditor.Editor
	{
        public override void OnInspectorGUI()
        {
            DungeonArchetype archetype = target as DungeonArchetype;

            if (archetype == null)
                return;

            EditorGUILayout.BeginVertical("box");

            EditorUtil.DrawIntRange("Branching Depth", archetype.BranchingDepth);
            EditorUtil.DrawIntRange("Branch Count", archetype.BranchCount);

            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorUtil.DrawObjectList<TileSet>("Tile Sets", archetype.TileSets, GameObjectSelectionTypes.Prefab);

			if(GUI.changed)
				EditorUtility.SetDirty(archetype);
        }
	}
}
