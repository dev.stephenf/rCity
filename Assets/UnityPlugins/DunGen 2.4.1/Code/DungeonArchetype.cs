﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace DunGen
{
    /// <summary>
    /// A description of the layout of a dungeon
    /// </summary>
    [Serializable]
	public sealed class DungeonArchetype : ScriptableObject
	{
        /// <summary>
        /// A collection of tile sets from which rooms will be selected to fill the dungeon
        /// </summary>
        public List<TileSet> TileSets = new List<TileSet>();
        /// <summary>
        /// The maximum depth (in tiles) that any branch in the dungeon can be
        /// </summary>
        public IntRange BranchingDepth = new IntRange(2, 4);
        /// <summary>
        /// The maximum number of branches each room can have
        /// </summary>
        public IntRange BranchCount = new IntRange(0, 2);
    }
}
